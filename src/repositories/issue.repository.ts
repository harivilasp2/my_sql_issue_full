import {DefaultCrudRepository} from '@loopback/repository';
import {Issue, IssueRelations} from '../models';
import {MysqldataDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class IssueRepository extends DefaultCrudRepository<
  Issue,
  typeof Issue.prototype.id,
  IssueRelations
> {
  constructor(
    @inject('datasources.mysqldata') dataSource: MysqldataDataSource,
  ) {
    super(Issue, dataSource);
  }
}
